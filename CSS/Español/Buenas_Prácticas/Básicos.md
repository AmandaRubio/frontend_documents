# Bases de CSS

## ¿Qué es CSS?
CSS significa _'Cascading Style Sheets'_ u _'Hojas de estilo en cascada'_, indicando el concepto de la integración de estilo a uno o varios documentos y de una lectura en caída (de arriba a abajo).

Siendo un tipo de lenguaje de programación que los navegadores como Chrome, Firefox, Explorer, entre otros, pueden comprender. Este tipo de código es considerado un componente o un agente secundario para un documento HTML puesto que permite módificarlo y darle múltiples atributos como color, forma y posición.

CSS permite la separación de diseño de una página web a un documento propio, dejando así que un documento HTML sea enfocado en el uso de datos y distribución de información, mientras que la hoja de estilos se encargará del aspecto visual. Esto da paso a distintias ventajas:

- Mayor organización.
- Mayor accesibilidad al diseño.
- Búsqueda de contenido sencilla.
- Menor esfuerzo en modificación visual de multiples páginas.

## ¿Cómo funciona?
Un documento u hoja de estilo CSS representa una plantilla de diseño que será implementada en un HTML, esto se logra al enlazar el documento CSS en HTML con la etiqueta 'link'. 

Por ejemplo:
<br>
Al enlazando un documento interno/propio: 
```html
<link rel="stylesheet" href="ejemplo_1.css">
```
<br>
O un documento externo/ajeno:
```html
<link rel="stylesheet" href="https://aaaa.com/css/stylesheet.css">
```

La propiedad **rel** permite definir el material que recibe, en este caso un _stylesheet_. Mientras que **href** enlaza la ubicación CSS, la cual puede ser de un documento propio o ajeno como se muestra en los ejemplos. Una vez enlazado, el documento HTML seguirá el reglamento de estilo definido por la hoja de estilos, y esto se mostrará de manera visual en el navegador o simulador.

## ¿En qué consiste?
Lo que define a CSS es el conjunto de reglas, las cuales son definidas por **declaraciones** dadas a **selectores**. 

### Selectores
Son las referencias HTML que llaman elementos específicos y nos permite cambiar su aspecto, pero no su información. Para llamar dichas referencias, existen distintos selectores que se representan según su tarea o nombre.

- Universal: *
    - Afecta a toda la página.
- Elemento: body
    - Afecta unicamente al elemento o etiqueta HTML llamada. 
- Atributo: [attr] a[href=“https://aa.com”]
    - Afecta a todo elemento que contenga el atributo especificado.
- Clase: .myClass
    - Su efecto es para toda etiqueta HTML con la clase llamada.
- Pseudo-clases: a:hover
    - Se utiliza para definir un estado particular de un elemento.
- Hermandad: 
    - Adyacente: div + p 
        - Se espera que el efecto se muestre en el segundo selector, según su posición en código. Por ejemplo, tomará el siguiente _p_ después de tu _div_.
    - General: div ~ p
        - Tomará todo p que este junto al _div_, pero no dentro.  
- ID: #myId
    - Su efecto es para toda etiqueta HTML con el ID llamado. 
- Descendiente: div p a
    - Afecta al último elemento de la cadena descendiente.
- Mix: div .contenido #articulo_01
    - Afecta al último elemento de la cadena mix.

Cada uno es llamado por una tarea o requerimiento específico, siendo estos requerimientos divididos como:

- De base: <br>
Establecer atributos base que el HTML seguirá, como el tamaño de la página, su margen o tipografía principal.
- Generales: <br>
Especificando elementos generales o que se repiten multiples veces, como el tamaño de titulos, de texto, tamaño de botones.
- Específico o por página: <br>
Aquellos atributos que se encuentran unicamente dentro de una sección, por ejemplo: Mi página principal posee botones azules, mientras que mi página de contacto posee botones verdes.
- Único o irrepetible: <br>
Atributo que se le da a un solo objeto y que no se repite en ningún otro momento.

### Declaraciones
Las declaraciones combinan **propiedades** y **valores** para darle atributos a los selectores. 

#### Propiedades
Cada propiedad atribuye a un elemento que puede ser modificado o añadido a un selector dentro de HTML. Son múltiples las propiedades que pueden ser manejadas dentro de CSS, por lo que en la siguiente lista de ejemplo se harán referencia de tres por tipo.

- De fuentes
    1. Font family
    2. Font style
    3. Font weight
- De color y fondo
    1. Color
    2. Background color
    3. Background image
- De texto
    1. Text decoration
    2. Text alignment
    3. Word Spacing
- De cuadro o espacio
    1. Margin
    2. Padding
    3. Border
- De clasificación
    1. Display
    2. Whitespace
    3. List Style Position
- Unidades
    1. Url
    2. Unidades de color
    3. Unidades de porcentaje

#### Valores
Son los elementos asignados a las propiedades que permiten especificar o modificar el HTML, 
Por ejemplo:
<br>
"Mi botón es verde"
`button { background-color: green } `
<br>
"Mi titulo es color rojo, con texto a 20px"
`h1{ color: red; font-size: 20px }` 


## Ejemplo práctico
Se presenta el siguiente ejemplo para mostrar su uso en código y resolver dudas de implementación.
El objetivo deseado es imitar la siguiente imagen: 
<br>
![Imagen ejemplo para CSS Básico](./../Images/Example_1_CSS.png "Ejemplo para CSS Básico")

En dicha imagen solo tenemos tres contendores que, por posición, asumimos que es un navegador, una sección principal y una adyacente, y como se encuentran vacíos solo se colocará un salto de línea para obtener espacio en blanco.

> Documento HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejemplo 1</title>
    <link rel="stylesheet" href="CSS_Basics_Example.css">
</head>
<body>
    <header>
        <nav>
            <br>
        </nav>
    </header>
    <main>
        <aside id="contenedor_1">
            <br>
        </aside>
        <section id="contenedor_2">
            <br>
        </section>
    </main>
</body>
</html>
```

Al tener pocos elementos, se definirán los atributos de manera general por sus etiquetas HTML, especificando unicamente aquellos que tienen ID para darles un valor único. En este caso también existen elementos que necesitan valores iguales, por los que se les asignarán dichos atributos en una sección única, separandolos con una coma (,). 

> Documento CSS

```css
body{
    margin:0;
    padding:0;
    background-color: lightgray;
}
header, main{
    margin: 30px 160px;
}
header, aside, section{
    background-color: #A4A3FF; 
}
header{
    height: 80px;
}
main{
    display: flex;
    flex-wrap: nowrap;
}
aside, section{
    height: 400px;
}
aside{
    width: 40%;
}
section{
    width: 60%;
}
#contenedor_1{
    margin-right: 5px;
}
#contenedor_2{
    margin-left:5px;
}
```
Con este código se deberá obtener una replica de la imagen pedida, se recomienda investigar aquellos datos y/o elementos que causen duda para su mejor comprensión, así como la experimentación con dicho código para una mayor práctica. 

### Referencias
- Manz, (2017). ¿Que es CSS?. Abril 2019, de @Manz Sitio web: https://lenguajecss.com/p/css/introduccion/que-es-css#disqus_thread
- López, B. (2019). La guía rápida para aprender CSS básico desde cero, de Ciudadano 2.0 Sitio web: 
https://www.ciudadano2cero.com/aprender-css-basico-desde-cero/
- Diario BF. (2014). Tutorial CSS3: El uso de reglas. Sitio web: https://www.dariobf.com/tutorial-css3-regla-cs/
- CSS Basics, (2019). CSS Introduction. Web site: https://www.cssbasics.com/